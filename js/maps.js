   jQuery(function($) {
          // Asynchronously Load the map API 
          var script = document.createElement('script');
          script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyA_q8I-ipfDo6YM8DQsV3AoaAis1Ai8bgE&callback=initialize";
          document.body.appendChild(script);
      });

      function initialize() {
          var map;
          var bounds = new google.maps.LatLngBounds();
          var mapOptions = {
              mapTypeId: 'roadmap'
          };
                          
          // Display a map on the page
          map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
          map.setTilt(45);
              
          // Multiple Markers
          var markers = [
              ['Centro Dentro , Torino', 45.0343562,7.635451900000021],
              ['Ferro di Cavallo , Parco Dora, Torino', 45.0896971,7.666719000000057],
              ['30 - Progetto MurArte, Torino',45.0599882,7.651761999999962],
              ['Hall of Fame CSOA Gabrio, Torino',45.0589759,7.649013599999989],
              ['Riqualificazione Area, Torino',45.0170951,7.638562900000011]
          ];
                              
          // Info Window Content
          var infoWindowContent = [
              ["<div class='info_content'><h3>Centro Dentro</h3></div>"],
              ["<div class='info_content'><h3>Ferro di Cavallo</h3></div>"],
              ["<div class='info_content'><h3>30 - Progetto MurArte</h3></div>"],
              ["<div class='info_content'><h3>Hall of Fame CSOA Gabrio</h3></div>"],
              ["<div class='info_content'><h3>Riqualificazione Area</h3></div>"],
              ["<div class='info_content'><h3>London Eye</h3></div>"]
          ];
              
          // Display multiple markers on a map
          var infoWindow = new google.maps.InfoWindow(), marker, i;
          
          // Loop through our array of markers & place each one on the map  
          for( i = 0; i < markers.length; i++ ) {
              var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
              bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  map: map,
                  title: markers[i][0]
              });
              
              // Allow each marker to have an info window    
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                      infoWindow.setContent(infoWindowContent[i][0]);
                      infoWindow.open(map, marker);
                  }
              })(marker, i));

              // Automatically center the map fitting all markers on the screen
              map.fitBounds(bounds);
          }

          // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
          var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
              this.setZoom(12);
              google.maps.event.removeListener(boundsListener);
          });
          
      }