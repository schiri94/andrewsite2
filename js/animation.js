$(document).ready(function(){
	$(".progress-bar-danger").animate({"width":"96"},500).html("30%");
	$(".progress-bar-warning").animate({"width":"32"},1000).html("10%");
	$(".progress-bar-success").animate({"width":"224"},1500).html("70%");
	$(".progress-bar-primary").animate({"width":"288"},2000).html("90%");
	$(".progress-bar-info").animate({"width":"208"},2500).html("65%");

	$(".btn-default, .btn-default .fa-search").click(function(){
		$("body").addClass("overlay_search");
		$(".overlay_search").css({
			"prosition":"fixed",
			"width":"100%",
			"height":"100%",
			"background-color":"#272B38",
			"color":"fff",
			"opacity":"0.8",
			"text-transform":"capitalize"
		});
		$(".overlay_search").html("<div class='close_search' onclick='close_overlay_search();' style='display: block;float: right;margin-top: 30px;margin-right: 30px;color: white;'><br><i class='fa fa-times fa-3x'></i></div><img src='img/logo.png' style='margin-top: 22px;height: 200px;'><p style='color:white;font-weight:400;font-size:30px;text-align:center'>StileLiber</p><h3 style='margin-top:100px;text-align: center;float: left;color: white;width: 100%;'>Nessun risultato trovato!</h3>")

	});
});
function close_overlay_search(){
			$(".overlay_search").fadeOut(500);
			$("body").removeClass("overlay_search");
			window.location.href = window.location.pathname.split("/").slice(-1)[0];
};